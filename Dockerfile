FROM debian:buster-backports AS build
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        build-essential pkg-config git ca-certificates openvpn \
&& DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends -t buster-backports golang-go \
&& rm -rf /var/lib/apt/lists/*

# don't need to do bash tricks to keep the layers small, as this is a multi-stage build
ENV GOPATH=/go
WORKDIR $GOPATH
RUN git clone https://github.com/OperatorFoundation/shapeshifter-dispatcher /shapeshifter-dispatcher && cd /shapeshifter-dispatcher && go build
RUN strip /shapeshifter-dispatcher/shapeshifter-dispatcher
RUN go get -u github.com/kumina/openvpn_exporter
RUN strip $GOPATH/bin/openvpn_exporter

FROM registry.git.autistici.org/ai3/docker/chaperone-base:stretch

# needed for buster, but chaperone-base image is still stretch
#RUN update-alternatives --set iptables /usr/sbin/iptables-legacy

COPY --from=build /shapeshifter-dispatcher/shapeshifter-dispatcher /usr/local/bin/shapeshifter-dispatcher
COPY --from=build /usr/sbin/openvpn /usr/sbin/openvpn
COPY --from=build /go/bin/openvpn_exporter /usr/local/bin/openvpn_exporter
COPY chaperone.d/ /etc/chaperone.d
RUN echo "deb http://download.opensuse.org/repositories/home:/CZ-NIC:/knot-resolver-latest/Debian_9.0/ /" > /etc/apt/sources.list.d/knot.list
COPY cznic-obs.gpg /etc/apt/trusted.gpg.d
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
        openvpn libcap2-bin netcat-openbsd iptables knot-resolver knot-resolver-module-http \
        && rm -rf /var/lib/apt/lists/*
RUN setcap cap_net_admin,cap_net_bind_service+ep /usr/sbin/openvpn
RUN setcap cap_net_admin+ep /bin/ip
RUN setcap cap_net_bind_service+ep /usr/sbin/kresd
